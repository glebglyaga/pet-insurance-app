const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const path = require('path');
const webpack = require('webpack');
const historyApiFallback = require('connect-history-api-fallback');

function getDefinePlugin(environment) {
  const config = {
    DEBUG: JSON.stringify(JSON.parse(environment.DEBUG || 'true'))
  };

  return new webpack.DefinePlugin(config);
}

module.exports = {
  context: path.resolve(__dirname, 'src'),
  entry:   './index.js',
  output:  {
    filename: 'pet-insurance-app.js',
    library:  'PetInsuranceApp',
    path:     path.resolve(__dirname, 'dist')
  },

  plugins: [
    new BrowserSyncPlugin({
      host:   'localhost',
      port:   '7000',
      open:   false,
      server: {
        baseDir:    [ 'www', 'dist' ],
        middleware: [ historyApiFallback() ]
      }
    }),

    getDefinePlugin(process.env),

    new MiniCssExtractPlugin({ filename: 'style.css' })
  ],
  module: {
    rules: [ {
      test:    /\.js$/,
      include: [
        path.resolve(__dirname, 'src')
      ],
      exclude: /node_modules/,
      use:     'babel-loader',
    }, {
      test:    /\.scss$/,
      include: [
        path.resolve(__dirname, 'style')
      ],
      exclude: /node_modules/,
      use:     [ MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader' ]
    } ]
  }
};
