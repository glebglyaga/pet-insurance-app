import Logger from './logger';

export default class FsaReducer {
  /**
   * Custom reducer optimized to work with Flux Standard Actions.
   * Prevents stressing reducers which are not responding to a provided action.
   * Also, it does not remove initial state fields which do not have reducers.
   *
   * @param {Object} reducers
   * @param {Object} actions
   */
  constructor(reducers, actions) {
    this.logger = Logger.createTaggedLoggers('FsaReducer');
    this.handledActions = this.getActionsMesh(reducers, actions);
  }

  create() {
    return (state, action) => {
      let reducer = this.handledActions[action.type];
      let currentSliceState, nextSliceState, sliceName;
      let finalState = state;

      if (reducer !== undefined) {
        sliceName = reducer.name;
        currentSliceState = state[sliceName];
        nextSliceState = reducer.routine(currentSliceState, action);

        if (currentSliceState !== nextSliceState) {
          // Shallow copy of the current state
          finalState = { ...state };
          finalState[sliceName] = nextSliceState;
        }
      }

      return finalState;
    };
  }

  /**
   * Lookup for a reducer which potentially responds to the provided action type.
   * Reducers should only manipulate the state without deep logic or side-effects.
   *
   * @param {Object} reducers all available reducers
   * @param {String} type Flux Standard Action type
   *
   * @returns {Object} reducer metadata with "name" and "routine" keys, where "routine" responds to the provided action type
   */
  findReducer(reducers, type) {
    let probeState = [];
    let probeAction = { type, meta: [], payload: [] };
    let result = null;
    let reducer, newState;

    for (let reducerName in reducers) {
      if (reducers.hasOwnProperty(reducerName) === true) {
        reducer = reducers[reducerName];
        newState = reducer(probeState, probeAction);
        if (newState !== probeState) {
          result = {
            name: reducerName,
            routine: reducer
          };
          break;
        }
      }
    }

    return result;
  }

  getActionsMesh(reducers, actions) {
    let actionType, reducer;
    let actionsMesh = {};

    for (let key in actions) {
      if (actions.hasOwnProperty(key) === true) {
        actionType = actions[key];
        reducer = this.findReducer(reducers, actionType);

        if (reducer !== null) {
          actionsMesh[actionType] = reducer;
        }

        if (DEBUG) {
          if (reducer === null) {
            this.logger.warn(key + ' action does not have a reducer.');
          }
        }
      }
    }

    // TODO Add Debug message with reducers which don't have relationship to provided Actions

    return actionsMesh;
  }
}
