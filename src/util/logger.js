import { leadingZero } from '../util/utils';

export default class Logger {

  static formatMessage(component, message) {
    return `[${Logger.currentTimeString()}] [${component}]: ${message}`;
  }

  static createTaggedLogger(tag, level = 'log') {
    const componentOrTag = tag;
    return (message, ...values) => {
      Logger[level].apply(Logger, [].concat(componentOrTag, message, values));
    };
  }

  static createTaggedLoggers(tag) {
    return {
      log:   Logger.createTaggedLogger(tag, 'log'),
      warn:  Logger.createTaggedLogger(tag, 'warn'),
      error: Logger.createTaggedLogger(tag, 'error')
    };
  }

  static currentTimeString() {
    const date = new Date();
    const hours = leadingZero(date.getHours(), 2),
          minutes = leadingZero(date.getMinutes(), 2),
          seconds = leadingZero(date.getSeconds(), 2),
          milliseconds = leadingZero(date.getMilliseconds(), 3);
    return `${hours}:${minutes}:${seconds}.${milliseconds}`;
  }

  static error(component, message, ...values) {
    Logger.pushMessage.apply(Logger, [].concat('error', Logger.formatMessage(component, message), values));
  }

  static log(component, message, ...values) {
    Logger.pushMessage.apply(Logger, [].concat('log', Logger.formatMessage(component, message), values));
  }

  static pushMessage(level, ...message) {
    /* eslint-disable no-console */
    console[level].apply(console, message);
  }

  static warn(component, message, ...values) {
    Logger.pushMessage.apply(Logger, [].concat('warn', Logger.formatMessage(component, message), values));
  }
}
