/**
 * Calculate value with provided random addition.
 *
 * @param {number} value
 * @param {number} length
 */
export function leadingZero(value, length) {
  value = value.toString();

  while (value.length < length) {
    value = '0' + value;
  }
  return value;
}

export function getData(url = '') {
  return fetch(url)
    .then(response => response.json());
}

export function postData(url = '', data = {}) {
  return fetch(url, {
    method:  'POST',
    cache:   'no-cache',
    headers: {
      'Content-Type': 'application/json',
    },
    referrer: 'no-referrer',
    body:     JSON.stringify(data),
  })
    .then(response => response.json());
}