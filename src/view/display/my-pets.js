import PropTypes from 'prop-types';
import React from 'react';

export function MyPets({ pets }) {
  return (
    <div className='my-pets'>
      <h1>My Pets</h1>
      <table>
        <thead>
          <tr>
            <th>Name</th>
            <th>Pet Type</th>
            <th>Breed</th>
            <th>Age</th>
          </tr>
        </thead>
        <tbody>
          {pets.map(({ id, petType, name, age, breed }) => (
            <tr key={id}><td>{name}</td><td>{petType}</td><td>{breed}</td><td>{age}</td></tr>
          )
          )}
        </tbody>
      </table>
    </div>
  );
}

MyPets.propTypes = {
  pets: PropTypes.arrayOf(PropTypes.object),
};