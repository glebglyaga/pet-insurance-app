import PropTypes from 'prop-types';
import React from 'react';
import { withRouter } from 'react-router';

export function PetsSuccessDisplay({ match: { params: { name } } }) {
  return (
    <div className='pets-success'>Congrats on enrolling {name}!</div>
  );
}

PetsSuccessDisplay.propTypes = {
  match: PropTypes.object,
};

export const PetsSuccess = withRouter(PetsSuccessDisplay);
