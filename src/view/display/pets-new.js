import PropTypes from 'prop-types';
import React, { useEffect, useRef, useState } from 'react';
import { Redirect } from 'react-router-dom';

function usePrevious(value) {
  const ref = useRef();

  useEffect(() => {
    ref.current = value;
  });

  return ref.current;
}

export function PetsNew({ addPetHandler }) {
  const [ name, setName ] = useState('');
  const [ age, setAge ] = useState(0);
  const [ breed, setBreed ] = useState('');
  const [ petType, setPetType ] = useState('dog');

  function submit() {
    if (name && petType && !isNaN(age) && breed) {
      addPetHandler({ name, age, breed, petType });
    }
  }

  function keyDownHandler(event) {
    if (event.keyCode === 13) {
      submit();
    }
  }

  return (
    <div className='pets-new'>
      <h1>Add pet</h1>

      <div className='pets-new__input'>
        <input
          type='text'
          placeholder='Name*'
          autoFocus
          onChange={event => setName(event.target.value)}
          onKeyDown={keyDownHandler}
        />

        <select
          defaultValue='dog'
          onChange={event => setPetType(event.target.value)}
          onKeyDown={keyDownHandler}
        >
          <option value='dog'>Dog</option>
          <option value='cat'>Cat</option>
        </select>

        <input
          type='text'
          placeholder='Age*'
          onChange={event => setAge(event.target.value)}
          onKeyDown={keyDownHandler}
        />
        <input
          type='text'
          placeholder='Breed*'
          onChange={event => setBreed(event.target.value)}
          onKeyDown={keyDownHandler}
        />
      </div>

      <div className='pets-new__submit'>
        <input type="button" value="Add" onClick={() => submit()} />
      </div>
    </div>
  );
}

PetsNew.propTypes = {
  addPetHandler: PropTypes.func
};