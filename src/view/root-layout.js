import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { BrowserRouter, Link, Redirect, Route } from 'react-router-dom';

import { getPets } from '../model/selectors/selectors';
import { MyPets } from './display/my-pets';
import { PetsSuccess } from './display/pets-success';
import { PetsNew } from './display/pets-new';

function RootLayout({ pets, addPetHandler }) {
  return (
    <div className='root-layout'>
        <div className='nav'>
          <Link to='/my-pets'>My pets</Link>
          <Link to='/pets/new'>Add pet</Link>
        </div>

        <Route exact path='/' render={() => <Redirect to='/my-pets'/>}/>
        <Route path='/my-pets' render={() => <MyPets pets={pets}/>}/>
        <Route path='/pets/new' render={() => <PetsNew pets={pets} addPetHandler={addPetHandler}/>}/>
        <Route path='/pets/success/:name' render={() => <PetsSuccess/>}/>
    </div>
  );
}

export default connect(state => {
  return {
    pets: getPets(state)
  };
})(RootLayout);

RootLayout.propTypes = {
  pets:          PropTypes.arrayOf(PropTypes.object),
  addPetHandler: PropTypes.func
};