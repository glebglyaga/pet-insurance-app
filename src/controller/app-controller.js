import { ConnectedRouter } from 'connected-react-router'
import React from 'react';
import ReactDom from 'react-dom';
import { Provider } from 'react-redux';

import {
  fetchPets,
  postPet
} from '../action/actions';
import { createReduxStore, history } from '../model/redux-store';
import Logger from '../util/logger';
import RootLayout from '../view/root-layout';
import { getError, getPets } from '../model/selectors/selectors';
import storeObserver from '../util/store-observer';

export default class AppController {
  constructor(config) {
    this.logger = Logger.createTaggedLoggers('AppController');
    this.view = config.view;
    this.store = createReduxStore();
    this.storeObservers = [];

    DEBUG && this.logger.log('App initialized with config:', config, this.store);

    this.addListeners();
    this.bootstrap();
  }

  addListeners() {
    this.addObserver(getError, error => {
      if (error) {
        this.logger.error('Error: ', error);
      }
    });

    this.addObserver(getPets, pets => {
      if (pets !== null) {
        this.render();
      }
    });
  }

  addObserver(selector, listener) {
    this.storeObservers.push(storeObserver(this.store, selector, listener));
  }

  bootstrap() {
    this.store.dispatch(fetchPets());
  }

  dispose() {
    if (this.storeObservers !== null) {
      this.storeObservers.forEach(unSub => unSub());
      this.storeObservers = null;
    }

    this.store = null;
  }

  postPet(pet) {
    this.store.dispatch(postPet({...pet}));
  }

  render() {
    ReactDom.render(
      <Provider store={this.store}>
        <ConnectedRouter history={history}>
          <RootLayout addPetHandler={pet => this.postPet(pet)}/>
        </ConnectedRouter>
      </Provider>,
      this.view
    );
  }
}