import * as ActionTypes from '../model/action-types';
import * as Constants from '../model/constants';
import Logger from '../util/logger';
import { getData, postData } from '../util/utils';

import { push } from 'connected-react-router';

export function addPet(pet) {
  return {
    type:    ActionTypes.PET_DID_ADD,
    payload: pet
  };
}

export function fetchPets() {
  return dispatch => {
    getData(Constants.PETS_ENDPOINT)
      .then(result => {
        Logger.createTaggedLogger('Actions')('fetchPets:', result);

        dispatch(setPets(result));
      })
      .catch(err => dispatch(setError(err)));
  };
}

export function postPet({ petType, name, age, breed }) {
  return dispatch => {
    postData(Constants.PETS_ENDPOINT, { petType, name, age, breed })
      .then(result => {
        Logger.createTaggedLogger('Actions')('postPet:', result);

        dispatch(addPet(result));
        dispatch(push(`/pets/success/${name}`));
      })
      .catch(err => dispatch(setError(err)));
  };
}

export function setError(error) {
  return {
    type:    ActionTypes.ERROR_DID_OCCUR,
    payload: error
  };
}

export function setPets(pets) {
  return {
    type:    ActionTypes.PETS_DID_CHANGE,
    payload: pets
  };
}