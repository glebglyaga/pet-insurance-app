import AppController from './controller/app-controller';
import '../style/style.scss';

export function Main(config) {
  let appController = new AppController(config);

  this.dispose = () => {
    if (appController !== null) {
      appController.dispose();
    }

    appController = null;
  };
}
