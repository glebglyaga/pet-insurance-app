import { applyMiddleware, combineReducers, createStore } from 'redux';
import reduxThunkMiddleware from 'redux-thunk';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import { createBrowserHistory } from 'history';

import * as Reducers from './reducers';
export const history = createBrowserHistory();

export function createReduxStore() {
  return createStore(
    combineReducers({
      ...Reducers,
      router: connectRouter(history)
    }),
    applyMiddleware(reduxThunkMiddleware, routerMiddleware(history))
  );
}
