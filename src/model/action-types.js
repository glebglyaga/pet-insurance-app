export const ERROR_DID_OCCUR = 'errorDidOccur';
export const PETS_DID_CHANGE = 'petsDidChange';
export const PET_DID_ADD = 'petDidAdd';