import * as ActionTypes from './action-types';

export function error(state = null, action) {
  switch (action.type) {
    case ActionTypes.ERROR_DID_OCCUR:
      return action.payload;
    default:
      return state;
  }
}

export function pets(state = null, action) {
  switch (action.type) {
    case ActionTypes.PETS_DID_CHANGE:
      return action.payload;
    case ActionTypes.PET_DID_ADD:
      return [ ...state, action.payload ];
    default:
      return state;
  }
}