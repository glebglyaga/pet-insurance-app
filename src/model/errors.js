/**
 * Channel does not exist
 * @constant
 * @default
 * @type {string}
 */
export const UNKNOWN_ERROR = 'unknownError';