import { expect } from 'chai';
import Sinon from 'sinon';

import {
  setError,
} from '../../src/action/actions';
import {
  getError
} from '../../src/model/selectors/selectors';
import { createReduxStore, getInitialState } from '../../src/model/redux-store';
import storeObserver from '../../src/util/store-observer';

describe('Store Observer', () => {

  let store;

  beforeEach(() => {
    store = createReduxStore(getInitialState());
  });

  it('listens for changes', () => {
    let errorListener = Sinon.spy();

    storeObserver(store, getError, errorListener);
    store.dispatch(setError({ text: 'erorText' }));

    expect(errorListener.lastCall.args[0]).to.be.deep.equal({ text: 'erorText' });
  });

  it('does not trigger if there are no changes in the target node', () => {
    const errorListener = Sinon.spy();

    storeObserver(store, getError, errorListener);

    expect(errorListener.callCount).to.be.equal(0);
  });

  it('removes subscription via the passed callback', () => {
    const errorListener = Sinon.spy((data, unsub) => unsub());

    storeObserver(store, getError, errorListener);
    store.dispatch(setError({ text: 'errorText' }));
    store.dispatch(setError({ text: 'anotherErrorText' }));

    expect(errorListener.callCount).to.be.equal(1);
  });
});
