import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import React from 'react';
import { spy } from 'sinon';

import { PetsSuccessDisplay } from '../../src/view/display/pets-success';

describe('PetsSuccess', () => {
  let wrapper;

  it('renders confirmation with name', () => {
    wrapper = mount(
      <PetsSuccessDisplay
        match={{ params: { name: 'Some Name' } }}
      />
    );

    expect(wrapper.find('.pets-success')).to.be.present();
  });
});